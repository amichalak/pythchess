#!/usr/bin/python
# -*- coding: utf-8 -*-

from Position import *
from stockfish import Stockfish
stockfish = Stockfish("./stockfish-10-win/Windows/stockfish_10_x64")


def uncode_stockfish_move(st_str: str) -> Move:

    return uncode_location(st_str[0:2]), uncode_location(st_str[2:4])



def chess_game():
    position: Position = Position(starting_pieces, Color.WHITE)
    chessboard: ChessBoard = ChessBoard(position)

    while not chessboard.is_checkmate() and not chessboard.is_stalemate() and not chessboard.is_insufficient_material():
        print(chessboard.to_str())
        print(code_list([key for key, value in chessboard.legal_moves_dict().items() if value != []]))
        chosen_location: Tuple[int, int] = uncode_location(input())
        while chosen_location not in chessboard.legal_moves_dict().keys():
            print("impossible, try again")
            chosen_location: Tuple[int, int] = uncode_location(input())
        print(code_list(chessboard.legal_moves_dict()[chosen_location]))
        chosen_direction: Tuple[int, int] = uncode_location(input())
        while chosen_direction not in chessboard.legal_moves_dict()[chosen_location]:
            print("impossible, try again")
            chosen_direction: Tuple[int, int] = uncode_location(input())
        chessboard.make_a_move((chosen_location, chosen_direction))
        sys.stdout.write(100*"\n")
    print(chessboard.to_str())
    print("koniec gry. Wygraly {}".format(opposite_color(chessboard.position.to_move)))



def chess_game_with_comp():
    position: Position = Position(starting_pieces, Color.WHITE)
    chessboard: ChessBoard = ChessBoard(position)

    while not chessboard.is_checkmate() and not chessboard.is_stalemate() and not chessboard.is_insufficient_material():
        print(chessboard.to_str())
        print(code_list([key for key, value in chessboard.legal_moves_dict().items() if value != []]))
        chosen_location: Tuple[int, int] = uncode_location(input())
        while chosen_location not in chessboard.legal_moves_dict().keys():
            print("impossible, try again")
            chosen_location: Tuple[int, int] = uncode_location(input())
        print(code_list(chessboard.legal_moves_dict()[chosen_location]))
        chosen_direction: Tuple[int, int] = uncode_location(input())
        while chosen_direction not in chessboard.legal_moves_dict()[chosen_location]:
            print("impossible, try again")
            chosen_direction: Tuple[int, int] = uncode_location(input())
        chessboard.make_a_move((chosen_location, chosen_direction))
        sys.stdout.write(100*"\n")
        stockfish.set_fen_position(chessboard.position.to_FEN())
        chessboard.make_a_move(uncode_stockfish_move(stockfish.get_best_move()))
    print(chessboard.to_str())
    print("koniec gry. Wygraly {}".format(opposite_color(chessboard.position.to_move)))


chess_game_with_comp()
