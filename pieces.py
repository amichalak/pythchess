#!/usr/bin/python
# -*- coding: utf-8 -*-

from enum import Enum
from typing import List, Tuple, Dict
import abc
import sys
from copy import deepcopy


FILES_names: List[str] = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
RANKS_names: List[str] = ['1', '2', '3', '4', '5', '6', '7', '8']
Rank = [i+1 for i in range(8)]
File = deepcopy(Rank)

Location = Tuple[int, int]
vector = Tuple[int, int]
Move = Tuple[Location, Location]


class Color(Enum):
    WHITE: bool = True
    BLACK: bool = False


def opposite_color(color: Color) -> Color:
    if color is Color.WHITE:
        return Color.BLACK
    else:
        return Color.WHITE


def move_piece_with_vector(location: Location, vect: vector) -> Location:
    if location[0] + vect[0] in range(1, 9) and location[1] + vect[1] in range(1, 9):
        return location[0] + vect[0], location[1] + vect[1]
    else:
        return None


def uncode_location(str_location: str) -> Tuple[int, int]:
    return FILES_names.index(str_location[0])+1, int(str_location[1])


def code_location(location: Location) -> str:
    return str(FILES_names[location[0]-1]) + str(location[1])


def code_list(location_list: List[Location]) -> List[str]:
    str_list = []
    for location in location_list:
        str_list.append(code_location(location))
    return str_list


class Piece(object):
    def __init__(self, location: Location, color: Color, was_moved: bool = False):
        self.location = location
        self.color = color
        self.was_moved = was_moved

    @abc.abstractmethod
    #generuje zasady poruszania sie danej figury, bez analizy, czy dane ruchy są dozwolone
    def list_of_possible_moves(self) -> List[Location]:
        pass

    def move(self, file: int, rank: int):
        self.location = (file, rank)

    @abc.abstractmethod
    def symbol(self):
        pass


class Rook(Piece):

    def list_of_possible_moves(self) -> List[Location]:
        return [(rank, self.location[1]) for rank in Rank if rank is not self.location[0]] \
               + [(self.location[0], file) for file in File if file is not self.location[1]]

    def symbol(self) -> str:
        if self.color is Color.WHITE:
            return 'R'
        else:
            return 'r'


class Knight(Piece):

    def list_of_possible_moves(self) -> List[Location]:
        list_of_possible_moves: List[Location] = []
        vector_list = [(2, 1), (1, 2), (-2, 1), (1, -2), (2, -1), (-1, 2), (-2, -1), (-1, -2)]
        proposed: Location = move_piece_with_vector(self.location, vector_list[0])
        for vector in vector_list:
            proposed: Location = move_piece_with_vector(self.location, vector)
            if proposed is not None:
                list_of_possible_moves.append(proposed)
            proposed: Location = move_piece_with_vector(self.location, vector)

        return list_of_possible_moves

    def symbol(self) -> str:
        if self.color is Color.WHITE:
            return 'N'
        else:
            return 'n'


class King(Piece):

    def list_of_possible_moves(self) -> List[Location]:
        list_of_possible_moves: List[Location] = []
        vector_list = [(0, 1), (1, 1), (1, 0), (1, -1), (0, -1), (-1, -1), (-1, 0), (-1, 1)]
        for vector_ in vector_list:
            proposed: Location = move_piece_with_vector(self.location, vector_)
            if proposed is not None:
                list_of_possible_moves.append(proposed)

        return list_of_possible_moves

    def symbol(self) -> str:
        if self.color is Color.WHITE:
            return 'K'
        else:
            return 'k'


class Bishop(Piece):

    def list_of_possible_moves(self) -> List[Location]:
        list_of_possible_moves: List[Location] = []
        vector_list = [(1, 1), (-1, 1), (-1, -1), (1, -1)]
        proposed: Location = self.location
        for vector_ in vector_list:
            while True:
                proposed = move_piece_with_vector(proposed, vector_)
                if proposed is not None:
                    list_of_possible_moves.append(proposed)
                else:
                    break
            proposed = self.location

        return list_of_possible_moves

    def symbol(self) -> str:
        if self.color is Color.WHITE:
            return 'B'
        else:
            return 'b'


class Queen(Piece):
    def list_of_possible_moves(self) -> List[Location]:
        list_of_possible_moves: List[Location] = []
        vector_list = [(1, 1), (-1, 1), (-1, -1), (1, -1)]
        proposed: Location = self.location
        for vector_ in vector_list:
            while True:
                proposed = move_piece_with_vector(proposed, vector_)
                if proposed is not None:
                    list_of_possible_moves.append(proposed)
                else:
                    break
            proposed = self.location

        list_of_possible_moves += [(rank, self.location[1]) for rank in Rank if rank is not self.location[0]] \
               + [(self.location[0], file) for file in File if file is not self.location[1]]
        return list_of_possible_moves

    def symbol(self) -> str:
        if self.color is Color.WHITE:
            return 'Q'
        else:
            return 'q'


class Pawn(Piece):
    def symbol(self) -> str:
        if self.color is Color.WHITE:
            return 'P'
        else:
            return 'p'

    def list_of_possible_moves(self) -> List[Location]:
        if self.color is Color.WHITE:
            return [move_piece_with_vector(self.location, (0, 1))] + int(not self.was_moved) \
                   * [move_piece_with_vector(self.location, (0, 2))]
        else:
            return [move_piece_with_vector(self.location, (0, -1))] + int(not self.was_moved) \
                   * [move_piece_with_vector(self.location, (0, -2))]



starting_pieces: List[Piece] = [Rook((1, 1), Color.WHITE),
                                Knight((2, 1), Color.WHITE),
                                Bishop((3, 1), Color.WHITE),
                                Queen((4, 1), Color.WHITE),
                                King((5, 1), Color.WHITE),
                                Bishop((6, 1), Color.WHITE),
                                Knight((7, 1), Color.WHITE),
                                Rook((8, 1), Color.WHITE),
                                Pawn((1, 2), Color.WHITE, False),
                                Pawn((2, 2), Color.WHITE, False),
                                Pawn((3, 2), Color.WHITE, False),
                                Pawn((4, 2), Color.WHITE, False),
                                Pawn((5, 2), Color.WHITE, False),
                                Pawn((6, 2), Color.WHITE, False),
                                Pawn((7, 2), Color.WHITE, False),
                                Pawn((8, 2), Color.WHITE, False),
                                Rook((1, 8), Color.BLACK),
                                Knight((2, 8), Color.BLACK),
                                Bishop((3, 8), Color.BLACK),
                                Queen((4, 8), Color.BLACK),
                                King((5, 8), Color.BLACK),
                                Bishop((6, 8), Color.BLACK),
                                Knight((7, 8), Color.BLACK),
                                Rook((8, 8), Color.BLACK),
                                Pawn((1, 7), Color.BLACK, False),
                                Pawn((2, 7), Color.BLACK, False),
                                Pawn((3, 7), Color.BLACK, False),
                                Pawn((4, 7), Color.BLACK, False),
                                Pawn((5, 7), Color.BLACK, False),
                                Pawn((6, 7), Color.BLACK, False),
                                Pawn((7, 7), Color.BLACK, False),
                                Pawn((8, 7), Color.BLACK, False)]