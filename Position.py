#!/usr/bin/python
# -*- coding: utf-8 -*-

from Pieces import *


class Position:
    def __init__(self, pieces: List[Piece] = starting_pieces, to_move: Color = Color.WHITE):
        self.pieces = pieces
        self.to_move = to_move
        self.last_move = ''
        self.number_of_full_moves = 1
        self.kings_list = [piece for piece in pieces if type(piece) is King]

    def to_table(self, orientation: Color = Color.WHITE) -> List[List[str]]:
        table_representation: List[List[str]] = [[], [], [], [], [], [], [], []]
        for i in range(1, 9):
            for j in range(1, 9):
                was_assigned: bool = False
                for piece in self.pieces:
                    if piece.location[0] is j and piece.location[1] is i:
                        table_representation[i - 1].append(piece.symbol())
                        was_assigned = True
                        break
                if was_assigned is False:
                    table_representation[i-1].append('.')
        if orientation is Color.WHITE:
            table_representation.reverse()

        else:
            for row in table_representation:
                row.reverse()

        return table_representation


    """ notacja FEN, czyli jednoznaczne przedstawienie sytuacji na szachownicy. Do poprawy -
            roszady, en passent"""
    #IMPOROVE
    def to_FEN(self) -> str:
        table_representation: List[List[str]] = self.to_table(Color.WHITE)
        FEN: str = ''
        for elements in table_representation:
            empty_squares_counter: int = 0
            for element in elements:
                if str(element) != '.':
                    if empty_squares_counter != 0:
                        FEN += str(empty_squares_counter)
                    empty_squares_counter = 0
                    FEN += element
                else:
                    empty_squares_counter += 1
            if empty_squares_counter != 0:
                FEN += str(empty_squares_counter)
            if table_representation[7][7] != element:
                FEN += '/'
        FEN += ' '
        if self.to_move is Color.WHITE:
            FEN += 'w '
        else:
            FEN += 'b '
        FEN += "KQkq - 0 " + str(self.number_of_full_moves)
        return FEN

    def list_occupied_squares(self) -> List[Location]:
        return [piece.location for piece in self.pieces]

    def list_squares_occupied_by_black(self) -> List[Location]:
        return [piece.location for piece in self.pieces if piece.color is Color.BLACK]

    def list_squares_occupied_by_white(self) -> List[Location]:
        return [piece.location for piece in self.pieces if piece.color is Color.WHITE]

    def list_squares_occupied_by(self, color: Color) -> List[Location]:
        return [piece.location for piece in self.pieces if piece.color is color]

    def is_occupied(self, location: Location) -> bool:
        return location in self.list_occupied_squares()

    def is_occupied_by_white(self, location: Location) -> bool:
        return location in self.list_squares_occupied_by_white()

    def is_occupied_by_black(self, location: Location) -> bool:
        return location in self.list_squares_occupied_by_black()

    def is_occupied_by(self, location: Location, color: Color) -> bool:
        return location in self.list_squares_occupied_by(color)

    def are_accessible(self, piece: Piece) -> List[Location]:
        color = piece.color
        accessible: List[Location] = []
        if type(piece) in [Bishop, Rook, Queen]:
            for possible_square in piece.list_of_possible_moves():
                location = piece.location
                vect: vector = (possible_square[0] - location[0], possible_square[1] - location[1])
                abs_vect = abs(vect[0]), abs(vect[1])
                unfold_vector_f = vect[0]/max(abs_vect), vect[1]/max(abs_vect)
                unfold_vector = int(unfold_vector_f[0]), int(unfold_vector_f[1])
                location = move_piece_with_vector(location, unfold_vector)
                while True:
                    if location is None or self.is_occupied_by(location, color):
                        break
                    if self.is_occupied_by(location, opposite_color(color)):
                        if location[0] == possible_square[0] and location[1] == possible_square[1]:
                            accessible.append(possible_square)
                            break
                        else:
                            break
                    if location[0] == possible_square[0] and location[1] == possible_square[1]:
                        accessible.append(possible_square)
                        break
                    location = move_piece_with_vector(location, unfold_vector)
            return accessible

        if type(piece) is Knight:
            return [location for location in piece.list_of_possible_moves() if location
                    not in self.list_squares_occupied_by(piece.color)]

        if type(piece) is Pawn:
            if color is Color.WHITE:
                possible_square = move_piece_with_vector(piece.location, (1, 1))
                if possible_square is not None:
                    if self.is_occupied_by(possible_square, opposite_color(piece.color)):
                        accessible.append(possible_square)
                possible_square = move_piece_with_vector(piece.location, (-1, 1))
                if possible_square is not None:
                    if self.is_occupied_by(possible_square, opposite_color(piece.color)):
                        accessible.append(possible_square)
                return accessible + int(not self.is_occupied(move_piece_with_vector(piece.location, (0, 1)))) * \
                       [move_piece_with_vector(piece.location, (0, 1))] + \
                       int (not piece.was_moved) * int(not self.is_occupied(move_piece_with_vector(piece.location, (0, 1)))) * \
                       int(not self.is_occupied(move_piece_with_vector(piece.location, (0, 2)))) * \
                       [move_piece_with_vector(piece.location, (0, 2))]
            if color is Color.BLACK:
                possible_square = move_piece_with_vector(piece.location, (1, -1))
                if possible_square is not None:
                    if self.is_occupied_by(possible_square, opposite_color(piece.color)):
                        accessible.append(possible_square)
                possible_square = move_piece_with_vector(piece.location, (-1, -1))
                if possible_square is not None:
                    if self.is_occupied_by(possible_square, opposite_color(piece.color)):
                        accessible.append(possible_square)
            return accessible + int(not self.is_occupied(move_piece_with_vector(piece.location, (0, -1)))) * \
                       [move_piece_with_vector(piece.location, (0, -1))] + \
                   int (not piece.was_moved) * \
                   int(not self.is_occupied(move_piece_with_vector(piece.location, (0, -1)))) * \
                       int(not self.is_occupied(move_piece_with_vector(piece.location, (0, -2)))) * \
                       [move_piece_with_vector(piece.location, (0, -2))]

        if type(piece) is King:
            return [possible_location for possible_location in piece.list_of_possible_moves() if not
            self.is_occupied_by(possible_location, piece.color)]

    def are_accessible_for_black(self) -> List[Location]:
        l: List[List[Location]] = [self.are_accessible(piece) for piece in self.pieces if piece.color is Color.BLACK]
        return [location for locations in l for location in locations]

    def are_accessible_for_white(self) -> List[Location]:
        l: List[List[Location]] = [self.are_accessible(piece) for piece in self.pieces if piece.color is Color.WHITE]
        return [location for locations in l for location in locations]

    def are_accessible_for(self, color: Color) -> List[Location]:
        l: List[List[Location]] = [self.are_accessible(piece) for piece in self.pieces if piece.color is color]
        return [location for locations in l for location in locations]

    def is_check(self):
        kings_list: List[Piece] = self.kings_list
        for king in kings_list:
            if king.location in self.are_accessible_for(opposite_color(king.color)):
                return True
        return False

    def in_check(self, color: Color):
        kings_list: List[Piece] = self.kings_list
        for king in kings_list:
            if king.color is color:
                if king.location in self.are_accessible_for(opposite_color(color)):
                    return True
        return False

    def promote_a_pawn(self, pawn: Pawn):
        self.pieces.append(Queen(pawn.location, pawn.color))
        self.pieces.remove(pawn)
        del pawn


class ChessBoard:
    def __init__(self, position: Position):
        self.position = position

    def legal_moves(self) -> List[Move]:
        legal_moves: List[Move] = []
        for piece in self.position.pieces:
            possible_moves: List[Move] = [(piece.location, possible_location) for possible_location in
                              self.position.are_accessible(piece) if piece.color is self.position.to_move]
            legal_moves.extend([moves for moves in possible_moves if self.is_legal(piece, moves)])

        return legal_moves

    def legal_moves_dict(self) -> Dict[Location, List[Location]]:
        legal_moves: List[Move] = self.legal_moves()
        legal_moves_dict: Dict[Location, List[Location]] = {}
        for occupied_squares in self.position.list_squares_occupied_by(self.position.to_move):
            legal_moves_dict[occupied_squares] = []
        for move in legal_moves:
            legal_moves_dict[move[0]].append(move[1])
        return legal_moves_dict

    def is_illegal(self, piece: Piece,  move: Move):
        piece.move(move[1][0], move[1][1])
        if self.position.is_occupied_by(piece.location, opposite_color(piece.color)):
            for pieces in self.position.pieces:
                if pieces.location == piece.location and pieces.color is opposite_color(piece.color):
                    pieces.move(pieces.location[0] + 2, pieces.location[1] + 1)
                    if self.position.in_check(piece.color):
                        piece.move(move[0][0], move[0][1])
                        pieces.move(pieces.location[0] - 2, pieces.location[1] - 1)
                        return True
                    pieces.move(pieces.location[0] - 2, pieces.location[1] - 1)
                    piece.move(move[0][0], move[0][1])
                    return False
        if self.position.in_check(piece.color):
            piece.move(move[0][0], move[0][1])
            return True
        piece.move(move[0][0], move[0][1])
        return False

    def is_legal(self, piece: Piece, move: Move):
        return not self.is_illegal(piece, move)

    def is_check(self):
        return self.position.is_check()

    def is_checkmate(self):
        return not self.legal_moves() and self.is_check()

    def make_a_move(self, move: Move) -> bool:
        if move not in self.legal_moves():
            return False
        for piece in self.position.pieces:
            if move[0] == piece.location:
                if self.position.is_occupied_by(move[1], opposite_color(self.position.to_move)):
                    for pieces in self.position.pieces:
                        if move[1] == pieces.location:
                            self.position.pieces.remove(pieces)
                            break
                piece.move(move[1][0], move[1][1])
                self.position.last_move = piece.symbol() + FILES_names[move[1][0]-1] + RANKS_names[move[1][1]-1]
                self.position.to_move = opposite_color(piece.color)
                if type(piece) is King:
                    self.position.kings_list = [piece for piece in self.position.pieces if type(piece) is King]
                if type(piece) is King or type(piece) is Pawn:
                    piece.was_moved = True
        return True

    def is_stalemate(self):
        return not self.legal_moves() and not self.position.in_check(self.position.to_move)

    def to_str(self):
        symbols_list: List[str] = ["R", "N", "B", "K", "Q", "P",
                                   "r", "n", "b", "k", "q", "p", "."]
        lil_pieces_list: List[str] = [u"\u265C", u"\u265E", u"\u265D", u"\u265A", u"\u265B", u"\u265F",
                           u"\u2656", u"\u2658", u"\u2657", u"\u2654", u"\u2655", u"\u2659", u"\u26DA"]

        unicode_fullwidth_letters: List[str] = [u"\uFF21", u"\uFF22", u"\uFF23", u"\uFF24", u"\uFF25", u"\uFF26",
                                                u"\uFF27", u"\uFF28"]
        numbers_list: List[str] = [u"\uFF18", u"\uFF17", u"\uFF16", u"\uFF15", u"\uFF14", u"\uFF13", u"\uFF12", u"\uFF11"]

        str_repr: str = ""
        orientation: Color = self.position.to_move
        table: List[List[str]] = self.position.to_table(orientation)


        if orientation is Color.BLACK:
            unicode_fullwidth_letters.reverse()
            numbers_list.reverse()


        for num, row in enumerate(table):
            str_repr += str(numbers_list[num])
            str_repr += "|"
            for square in row:
                str_repr += lil_pieces_list[symbols_list.index(square)]
                str_repr += "|"

            str_repr += "\n"

        if self.is_checkmate():
            str_repr += u"\uFF03"
        else:
            if self.is_check():
                str_repr += u"\uFF0B"
            else:
                str_repr += u"\uFF0f"
        str_repr += "|"


        for letter in unicode_fullwidth_letters:
            str_repr += letter
            str_repr += "|"
        str_repr += "\n"

        return str_repr

    #IMPLEMENT
    def is_insufficient_material(self) -> bool:
        pass


# p1 = Position(starting_pieces, Color.WHITE)
# c1 = ChessBoard(p1)
# c1.make_a_move(((5, 2), (5, 4)))
# c1.make_a_move(((4, 7), (4, 6)))
# c1.make_a_move(((6, 1), (2, 5)))
# c1.make_a_move(((3, 8), (4, 7)))
# c1.make_a_move(((2, 5), (3, 4)))
# c1.make_a_move(((2, 8), (3, 6)))
# c1.make_a_move(((4, 1), (6, 3)))
# c1.make_a_move(((1, 7), (1, 5)))
# c1.make_a_move(((6, 3), (6, 7)))
# print(c1.to_str())

# def is_bound(self, piece: Piece) -> bool:
    #     piece_location: Location = piece.location
    #     color = piece.color
    #     for king in self.kings_list:
    #         if king.color is piece.color:
    #             kings_location: Location = king.location
    #     vect: vector = (kings_location[0] - piece_location[0], kings_location[1] - piece_location[1])
    #     abs_vect: vector = (abs(vect[0]), abs(vect[1]))
    #     unfold_vector_f = vect[0] / max(abs_vect), vect[1] / max(abs_vect)
    #     unfold_vector = int(unfold_vector_f[0]), int(unfold_vector_f[1])
    #     if unfold_vector not in [[(0, 1), (1, 1), (1, 0), (1, -1), (0, -1), (-1, -1), (-1, 0), (-1, 1)]]:
    #         return False
    #     else:
    #         check_location = move_piece_with_vector(kings_location, unfold_vector)
    #         while True:
    #             if check_location == piece_location:
    #                 break
    #             if self.is_occupied(check_location):
    #                 return False
    #             check_location = move_piece_with_vector(check_location, unfold_vector)
    #         check_location = move_piece_with_vector(check_location, unfold_vector)
    #         while True:
    #             if check_location is None or self.is_occupied_by(check_location, color):
    #                 return False
    #             if self.is_occupied_by(check_location, opposite_color(color)):
    #                 if unfold_vector in [(0, 1), (1, 0), (-1, 0), (0, -1)]:
    #                     return Rook in [type(piece) for piece in self.pieces] or\
    #                            Queen in [type(piece) for piece in self.pieces]
    #                 if unfold_vector in [(1, 1), (-1, 1), (1, -1), (-1, -1)]:
    #                     return Bishop in [type(piece) for piece in self.pieces] or \
    #                            Queen in [type(piece) for piece in self.pieces]
    #                 break
    #             check_location = move_piece_with_vector(check_location, unfold_vector)
    #     return False




